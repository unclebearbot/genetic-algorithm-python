import argparse
import logging
import random
import time

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s.%(msecs)03d %(levelname)s - %(message)s",
    datefmt="%H:%M:%S"
)
log = logging.getLogger()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--population", type=int, default=1000, help="the size of population")
    parser.add_argument("--selection", type=float, default=0.9, help="the selection ratio of each generation")
    parser.add_argument("--timeout", type=int, default=60, help="the timeout in second")
    parser.add_argument("--generation", type=int, default=10000, help="the max generation")
    parser.add_argument("--top", type=float, default=0.01, help="the top ratio of each generation")
    parser.add_argument("--output", type=str, default="output.txt", help="the output file")
    args = parser.parse_args()
    start(args.population, args.selection, args.timeout, args.generation, args.top, args.output)


def start(population_size, selection_ratio, timeout, generation, top_ratio, output_file):
    milestones = [
        int(generation * 0.1), int(generation * 0.2), int(generation * 0.3),
        int(generation * 0.4), int(generation * 0.5), int(generation * 0.6),
        int(generation * 0.7), int(generation * 0.8), int(generation * 0.9),
    ]
    log.info("Now generating the initial population, please wait")
    population = generate_population(population_size)
    log.info("Now starting evolution, please wait")
    start_time = time.time()
    end_time = start_time + timeout
    i = 0
    while True:
        now = time.time()
        if now >= end_time:
            log.info("Run out of %s seconds after %s generations", repr(timeout), repr(i))
            break
        if i >= generation:
            log.info("Completed %s generations in %s seconds", repr(generation), now - start_time)
            break
        if is_top_stabilized(population, top_ratio):
            log.info("Top %s%% population stabilized", repr(top_ratio * 100))
            break
        i += 1
        selected_population = select(population, selection_ratio)
        new_population = generate_population(population_size - len(selected_population))
        population = selected_population + new_population
        if i in milestones:
            log.info("Completed %s of %s generations, continuing", repr(i), repr(generation))
    log.info("Top %s%% population will be kept", repr(top_ratio * 100))
    population = select(population, top_ratio)
    log.info("Completed evolution")
    log.info("Now saving %d lines to %s, please wait", len(population), output_file)
    save_to_file(population, output_file)
    log.info("Saved %d lines", len(population))
    return


def generate_population(size):
    population = []
    while True:
        if len(population) >= size:
            break
        population.append(Individual())
    return population


def is_top_stabilized(population, ratio):
    top = select(population, ratio)
    for individual in top:
        if individual.get_fitness() != top[0].get_fitness():
            return False
    return True


def select(population, ratio):
    return sorted(population, key=lambda it: it.get_fitness(), reverse=True)[:int(len(population) * ratio)]


def save_to_file(population, file):
    file = open(str(file), "w")
    for individual in population:
        file.write(individual.to_string())
        file.write("\n")
    file.close()


class Individual:
    def __init__(self):
        while True:
            self.x1 = random.random()
            self.x2 = random.random() * (1 - self.x1)
            self.x3 = random.random() * (1 - self.x1 - self.x2)
            self.x4 = 1 - self.x1 - self.x2 - self.x3
            if self.is_valid():
                break

    def is_valid(self):
        return self.x1 >= 0 and self.x1 >= 0 and self.x1 >= 0 and self.x1 >= 0 and self.x1 + self.x2 + self.x3 + self.x4 == 1

    def get_y(self):
        return 32906277.22 * self.x1 ** 2       \
             + 34514969.19 * self.x2 ** 2       \
             + 29190000.46 * self.x3 ** 2       \
             + 34613158.58 * self.x4 ** 2       \
             + 67388324.55 * self.x1 * self.x2  \
             + 61979726.73 * self.x1 * self.x3  \
             + 67493891.61 * self.x1 * self.x4  \
             + 63476432.28 * self.x2 * self.x3  \
             + 69111148.28 * self.x2 * self.x4  \
             + 63562019.85 * self.x3 * self.x4  \
             - 33726588.36 * self.x1            \
             - 34541851.79 * self.x2            \
             - 31758602.89 * self.x3            \
             - 34602300.44 * self.x4            \
             + 34643645.95

    def get_fitness(self):
        return self.get_y() * -1

    def to_string(self):
        return "x1={}, x2={}, x3={}, x4={}, y={}".format(
            repr(self.x1), repr(self.x2), repr(self.x3), repr(self.x4), repr(self.get_y())
        )


if __name__ == "__main__":
    main()
